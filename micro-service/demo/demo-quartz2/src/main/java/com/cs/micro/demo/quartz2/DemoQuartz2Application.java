package com.cs.micro.demo.quartz2;

import com.cs.base.quartz.EnableQuartz;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DemoQuartz1Application
 * @since 2019-03-17 16:20
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableQuartz
public class DemoQuartz2Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoQuartz2Application.class, args);
    }

}
