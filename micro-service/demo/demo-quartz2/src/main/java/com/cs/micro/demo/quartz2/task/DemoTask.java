package com.cs.micro.demo.quartz2.task;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

/**
 * @author wangjiahao
 * @version 1.0
 * @className DemoTask
 * @since 2019-03-18 11:25
 */
@Component
public class DemoTask implements Job {

    @Override
    public void execute(JobExecutionContext context){
        System.out.println("@@@@@@demoTask run @@@@@@");
    }



}
